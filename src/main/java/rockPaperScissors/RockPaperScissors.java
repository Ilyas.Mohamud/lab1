package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
        
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public String CompChoice() {
    	int rnd = new Random().nextInt(rpsChoices.size());
    	
    	String compchoice = rpsChoices.get(rnd);
    	return compchoice;
	}
    
    
    
    public Boolean isWinner(String choice1, String choice2) {
    	//checks for what is chosen,
    	if (choice1.matches("rock")) {
    		return (choice2.matches("scissors"));
    	}
    	else if (choice1.matches("paper")) {
    		return (choice2.matches("rock"));
    	}
    	else {
    		return (choice2.matches("paper"));
    	}
		
    }
    
    public String InputValidator(String Input){
    	String Validinput = Input.toLowerCase();
    	return Validinput;
    }
    
    public String UserChoice() {
    	while (true){
    		System.out.println("Your choice (Rock/Paper/Scissors)?");
    		String inpute = sc.nextLine();
    		String correctinput = InputValidator(inpute);
    		if (rpsChoices.contains(correctinput)) {
    		return correctinput;
    		}
    		else {
    			System.out.println("I do not understand "+ correctinput + ". Could you try again?");
    		}
    	}
    }
    public Boolean quit() {
    	while (true){
    		System.out.println("Do you wish to continue playing? (y/n)?");
    		String inputdirt = sc.nextLine();
    		String input = InputValidator(inputdirt);
    		if (input.matches("y")) {
    			roundCounter+=1;
    			return true;
    		}
    		else if (input.matches("n")) {
    			System.out.println("Bye bye :)");
    			return false;
    		}
    		else {
    			System.out.println("I do not understand "+ input + ". Could you try again?");
    			
    			
    		}
    	}
    }
    
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	while (true) {
    		System.out.println("Let's play round " + roundCounter);
    		
    		String userC = UserChoice();
    		String compC = CompChoice();
    		
    		if (isWinner(userC, compC)) {
    			System.out.println("Human chose " + userC + ", computer chose " + compC + ". Human wins.");
    			humanScore +=1;
    		}
    		if (isWinner(compC, userC)) {
    			System.out.println("Human chose " + userC + ", computer chose " + compC + ". Computer wins.");
    			computerScore +=1;
    		}
    		if (!isWinner(userC, compC) & !isWinner(compC, userC)) {
    			System.out.println("Human chose " + userC + ", computer chose " + compC + ". It's a tie.");
    			;
    		}
    		System.out.println("Score: human " + humanScore + " computer " + computerScore );
    		
    		
    		Boolean continued = quit();
    		if (!continued) {
    			break;
    		}
    	}
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
        
    }

}
